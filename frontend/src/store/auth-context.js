import React, { useState } from "react";

export const Context = React.createContext();
export default function AuthContext(props) {
  const [isLoggedIn, setIsLoggedIn] = useState(
    localStorage.getItem("isLoggedIn") ? true : false
  );
  const [isAdmin, setIsAdmin] = useState(
    localStorage.getItem("isAdmin") ? true : false
  );

  const [data, setData] = useState();
  return (
    <Context.Provider
      value={{ isLoggedIn, setIsLoggedIn, isAdmin, setIsAdmin, data, setData }}
    >
      {props.children}
    </Context.Provider>
  );
}
