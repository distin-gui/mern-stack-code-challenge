import { React, useState, useContext } from "react";
import axios from "axios";
import styles from "./Register.module.css";
import logo from "../assets/images/chef.png";
import image from "../assets/images/chef.png";
import { useNavigate } from "react-router-dom";
import { Context } from "../store/auth-context";

const Register = () => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
    name: "",
  });
  // const { email, password, name } = formData;

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const API_URL = process.env.REACT_APP_BACK_URL + `/api/users`;
  let navigate = useNavigate();
  const ctx = useContext(Context);
  const reg = async () => {
    const res = await axios.post(API_URL, formData);

    if (res.data.token) {
      ctx.setIsLoggedIn(true);
      localStorage.setItem("token", res.data.token);
      localStorage.setItem("name", res.data.name);
      localStorage.setItem("isLoggedIn", true);
      localStorage.setItem("email", res.data.email);
      localStorage.setItem("_id", res.data._id);
      navigate("/", { replace: true });
    }
    return res.data;
  };
  const onSubmit = (e) => {
    e.preventDefault();
    reg();
  };

  return (
    <>
      <div className={styles.Register__container}>
        <div className={styles.Section__one}>
          <img src={logo} alt="logo" className={styles.logo}></img>
        </div>

        <div className={styles.Section__two}>
          <h1 className={styles.heading}>
            Register{" "}
            <img src={image} alt="image1" className={styles.image}></img>
          </h1>

          <section className={styles.login__form}>
            <form onSubmit={onSubmit}>
              <div className={styles.form__group}>
                <input
                  type="text"
                  id="email"
                  name="email"
                  placeholder="Enter your email"
                  onChange={onChange}
                />
              </div>

              <div className={styles.form__group}>
                <input
                  type="password"
                  id="password"
                  name="password"
                  placeholder="Enter your password"
                  onChange={onChange}
                />
              </div>
              <div className={styles.form__group}>
                <input
                  type="text"
                  id="name"
                  name="name"
                  placeholder="Enter your name"
                  onChange={onChange}
                />
              </div>
              {/* <div className={styles.form__group}>
                <input
                  type="number"
                  className={styles.small__input}
                  pattern="[0-9]+"
                  id="phonenumber"
                  name="phonenumber"
                  placeholder="Enter your phone number"
                  onChange={onChange}
                />
              </div> */}
              <div className={styles.form__group}>
                <button type="submit" style={{ justifyContent: "center" }}>
                  Submit
                </button>
              </div>
            </form>
          </section>
        </div>
      </div>
    </>
  );
};

export default Register;
