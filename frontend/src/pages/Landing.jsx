import { React, useState, useEffect, useContext } from "react";
import Spinner from "../components/Spinner";
import axios from "axios";
import styles from "./LandingPage.module.css";
import Menu from "../components/Menu";
import Items from "../components/Items";
import { Context } from "../store/auth-context";
//react smooth scroll for items section

const Landing = () => {
  //REST Config
  const API_URL = process.env.REACT_APP_BACK_URL + "/api/categories/";
  const token = localStorage.getItem("token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const ctx = useContext(Context);

  //REST call
  // eslint-disable-next-line no-unused-vars
  const [categories, setCategories] = useState();
  const [menu, setMenu] = useState();

  const getCategories = async () => {
    const response = await axios.get(API_URL, config);
    setCategories(response.data);
    let menuName = [];

    response?.data?.map((cat) => {
      menuName.push(cat.name);
      return setMenu([...menuName]);
    });

    ctx.setData(response.data);
    return response.data;
  };

  useEffect(() => {
    getCategories();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // useEffect(() => {
  //   const getItems = () => {
  //     let catItems = [];
  //     for (var i = 0; i < categories?.length; i++) {
  //       console.log(categories[i]);
  //       catItems.push(categories[i].items);
  //       setItems([...catItems]);
  //     }
  //   };
  //   getItems();
  // }, [categories]);
  return (
    <>
      {/* container */}
      <section className={styles.Landing__Page__MenuContainer}>
        {/* category component */}
        <h1 className={styles.Landing__Page__h1}>Our lovely Menu</h1>
        <ul className={styles.Landing__Page__ul}>
          {!menu ? (
            <Spinner />
          ) : (
            menu.map((catName, index) => {
              return (
                <>
                  <Menu key={index} catName={catName} />
                </>
              );
            })
          )}
        </ul>
      </section>
      {/* item component */}
      <section className={styles.Landing__Page__ItemsContainer}>
        {!categories ? (
          <Spinner />
        ) : (
          categories?.map((items, index) => {
            return (
              <>
                <h1 className={styles.catName}>{items?.name}</h1>
                {items?.items.map((itemsData) => {
                  return (
                    <Items
                      catName={items.name}
                      key={index}
                      name={itemsData?.name}
                      price={itemsData?.price}
                      image={itemsData?.image}
                      description={itemsData?.description}
                      id={itemsData?._id}
                    />
                  );
                })}
              </>
            );
          })
        )}
      </section>
    </>
  );
};

export default Landing;
