import { React, useState, useEffect } from "react";
import Spinner from "../components/Spinner";
import { IoAddCircle } from "react-icons/io5";
import axios from "axios";
import styles from "./Admin.module.css";
import Table from "../components/Table";
import Button from "@mui/material/Button";
import Autocomplete from "@mui/material/Autocomplete";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import { imageListItemBarClasses, TextField } from "@mui/material";
import { toast } from "react-toastify";

function Admin() {
  const [categoryData, setCategoryData] = useState({});
  const [itemsData, setItemsData] = useState([]);
  const [itemData, setItemData] = useState([]);
  const [allItems, setAllItems] = useState([]);
  const [items, setItems] = useState([]);
  const [categories, setCategories] = useState();
  const [catName, setCatName] = useState();
  const [menuItems, setMenuItems] = useState([]);
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [open3, setOpen3] = useState(false);
  const [value, setValue] = useState([]);
  const [categoryValue, setCategoryValue] = useState();

  const [uploadedImage, setUploadedImage] = useState([]);
  const imageChange = (e) => {
    e.preventDefault();
    if (e.target.files && e.target.files.length > 0) {
      setUploadedImage({
        imagePreview: URL.createObjectURL(e.target.files[0]),
        imageAsFile: e.target.files[0],
      });
      setItemData((params) => ({
        ...params,
        image: uploadedImage?.imageAsFile,
      }));
    }
  };
  const { image, name, price, description } = itemData;
  // eslint-disable-next-line no-unused-vars
  // const { name, price, description, image } = itemData;
  // eslint-disable-next-line no-unused-vars

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleClickOpen2 = () => {
    setOpen2(true);
  };

  const handleClose2 = () => {
    setOpen2(false);
    setCategoryValue({});
    setValue([]);
  };
  const handleClickOpen3 = () => {
    setOpen3(true);
  };
  const handleClose3 = () => {
    setOpen3(false);
  };

  const API_URL = process.env.REACT_APP_BACK_URL + "/api/";
  const token = localStorage.getItem("token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  // const ctx = useContext(Context);
  //REST call   redundant shoud have used context for apis too

  const getCategories = async () => {
    const response = await axios.get(API_URL + "categories/", config);
    setCategories(response.data);
    let temp = [];
    response.data.map((item, i) => {
      temp.push([{ name: item.name, id: item._id }]);
      return temp;
    });
    setItems([...temp.flat()]);
  };

  useEffect(() => {
    getCategories();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getAllItems = async () => {
    const response = await axios.get(API_URL + "items/", config);
    let temp = [];
    let temp2 = [];
    response.data.map((item, i) => {
      temp.push([item]);
      return temp;
    });
    setAllItems(temp);
    temp.map((item, i) => {
      return item.map((item, i) => {
        temp2.push([{ name: item.name, id: item._id }]);
        return temp2;
      });
    });
    setItemsData(temp2.flat());
  };

  useEffect(() => {
    getAllItems();
  }, []);
  const onChange = (e) => {
    setCategoryData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handleAddCategory = async (e) => {
    e.preventDefault();
    await axios.post(API_URL + "categories/", categoryData, config);

    handleClose();
    getCategories();
    toast.success("Category Added!");
    setCategoryData("");
  };
  const onItemChange = (e) => {
    setItemData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const handleAddItem = async (e) => {
    if (
      value.length <= 0 ||
      categoryValue === undefined ||
      categoryValue === null
    ) {
      toast.error("check Inputs");
    } else {
      let temp3 = value.map((item, i) => {
        return { id: item.id };
      });
      console.log(temp3);
      var temp4 = { items: value };
      console.log(temp4);
      e.preventDefault();
      await axios.put(
        API_URL + "categories/edit/" + categoryValue.id,
        temp4,
        config
      );
      toast.success("Menu Modified");
      getCategories();

      handleClose2();
    }
  };
  const handleAddItem2 = async (e) => {
    e.preventDefault();
    await axios.post(API_URL + "items/", itemData, config);
    handleClose3();
    getAllItems();
    toast.success("Item Added!");
    setItemData("");
  };
  const overflow = {
    overflow: "hidden",
    overflowY: "hidden",
    overflowX: "hidden",
    scrollbarWidth: "none" /* Firefox */,
  };
  const options = itemsData.map((item, i) => {
    return item;
  });

  return (
    <>
      <h1 style={{ padding: "0px 2rem" }}>Admin Dashboard</h1>
      <div className={styles.admin__table}>
        <div className={styles.table__header}>
          {" "}
          <h2 className={styles.title}>
            Category{" "}
            <IoAddCircle
              title="Add Category"
              size={40}
              className="iconss__func"
              onClick={handleClickOpen}
              color="var(--secondary)"
              cursor="pointer"
            />
          </h2>
          <Dialog
            fullWidth={true}
            sx={{ overflowY: "scroll" }}
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle>
              <h2>Add Category </h2>
            </DialogTitle>
            <DialogContent>
              <TextField
                margin="dense"
                fullWidth
                label="Category Name"
                variant="standard"
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={catName}
                onChange={onChange}
              />
            </DialogContent>{" "}
            <DialogActions>
              <Button onClick={handleClose}>Cancel</Button>
              <Button onClick={handleAddCategory}>Add</Button>
            </DialogActions>
          </Dialog>
        </div>
        <table className={styles.table}>
          <tbody>
            {!categories ? (
              <Spinner />
            ) : (
              categories.map((menu, i) => {
                return (
                  <Table
                    key={menu._id}
                    id={menu._id}
                    name={menu.name}
                    reFetch={getCategories}
                    isCat={true}
                  />
                );
              })
            )}
          </tbody>
        </table>
      </div>

      <div className={styles.admin__table}>
        <div className={styles.table__header}>
          <h2 className={styles.title}>
            Menu
            <IoAddCircle
              title="Create Menu"
              size={40}
              className="iconss__func"
              onClick={handleClickOpen2}
              color="var(--secondary)"
              cursor="pointer"
            />
          </h2>

          <Dialog
            fullWidth={true}
            sx={{ overflowY: "scroll" }}
            open={open2}
            onClose={handleClose2}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle>
              <h2>Add Items To Menu </h2>
            </DialogTitle>
            <DialogContent>
              <Autocomplete
                disablePortal
                id="combo-box-demo"
                options={items}
                value={categoryValue}
                onChange={(e, data) => setCategoryValue(data)}
                defaultValue={[items[0]]}
                getOptionLabel={(option) => option.name || ""}
                isOptionEqualToValue={(option, value) =>
                  value === undefined ||
                  value === "" ||
                  option.id === value.id ||
                  option.value === value.value
                }
                sx={{ width: 400, alignSelf: "center" }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    label="Select Category"
                  />
                )}
              />
            </DialogContent>
            <DialogContent>
              <Autocomplete
                multiple
                disablePortal
                id="combo-box-demo"
                options={options}
                value={value}
                onChange={(e, data) => setValue(data)}
                defaultValue={[options[0]]}
                getOptionLabel={(option) => option.name || ""}
                // isOptionEqualToValue={(option, value) =>
                //   value === undefined ||
                //   value === "" ||
                //   option.id === value.id ||
                //   option.value === value.value
                // }
                sx={{ width: 400, alignSelf: "center" }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    label="Select Items"
                  />
                )}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose2}>Cancel</Button>
              <Button onClick={handleAddItem}>Add</Button>
            </DialogActions>
          </Dialog>
        </div>

        <table className={styles.table}>
          <tbody>
            {!categories ? (
              <Spinner />
            ) : (
              categories.map((item, i) => {
                var catId = item._id;
                return (
                  <>
                    <h2 className={styles.itemTitle}>{item.name}</h2>

                    <div Catid={item._id}>
                      {item.items.map((item, i) => {
                        return (
                          <Table
                            key={item._id}
                            name={item.name}
                            id={item._id}
                            price={item.price}
                            reFetch={getCategories}
                            isCat={false}
                            isMenu={true}
                            catid={catId}
                          />
                        );
                      })}
                    </div>
                  </>
                );
              })
            )}
          </tbody>
        </table>
      </div>

      <div className={styles.admin__table}>
        <div className={styles.table__header}>
          <h2 className={styles.title}>
            Items{" "}
            <IoAddCircle
              title="Add Item"
              size={40}
              className="iconss__func"
              onClick={handleClickOpen3}
              color="var(--secondary)"
              cursor="pointer"
            />{" "}
          </h2>
          <Dialog
            fullWidth={true}
            sx={{ overflowY: "scroll" }}
            open={open3}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle>
              <h2>Add Items </h2>
            </DialogTitle>
            <DialogContent>
              <TextField
                margin="dense"
                fullWidth
                label="Item Name"
                variant="standard"
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={name}
                onChange={onItemChange}
              />
            </DialogContent>{" "}
            <DialogContent>
              <TextField
                margin="dense"
                fullWidth
                label="Item Price"
                variant="standard"
                type="number"
                className="form-control"
                id="price"
                name="price"
                value={price}
                onChange={onItemChange}
              />
            </DialogContent>{" "}
            <DialogContent>
              <TextField
                margin="dense"
                fullWidth
                label="Item Description"
                variant="standard"
                type="text"
                className="form-control"
                id="description"
                name="description"
                value={description}
                onChange={onItemChange}
              />
            </DialogContent>{" "}
            <DialogContent style={overflow}>
              <TextField
                type="file"
                id="image"
                name="image"
                accept="image/*"
                autoFocus
                margin="dense"
                fullWidth
                variant="standard"
                onInput={imageChange}
                onChange={imageChange}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose3}>Cancel</Button>
              <Button onClick={handleAddItem2}>Add</Button>
            </DialogActions>
          </Dialog>
        </div>
        <table className={styles.table}>
          <tbody>
            {!categories ? (
              <Spinner />
            ) : (
              allItems.map((items, i) => {
                return items.map((item, i) => {
                  return (
                    <Table
                      key={item._id}
                      id={item._id}
                      name={item.name}
                      price={item.price}
                      description={item.description}
                      image={item.image}
                      reFetch2={getAllItems}
                      isCat={false}
                    />
                  );
                });
              })
            )}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default Admin;
