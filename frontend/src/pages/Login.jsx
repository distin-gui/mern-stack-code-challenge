import { React, useState, useContext } from "react";
import styles from "./Login.module.css";
import image1 from "../assets/images/chef.png";
import { Context } from "../store/auth-context";
import { useNavigate } from "react-router-dom";
import axios from "axios";
// import image1 from "../assets/images/login.jpg";

const Login = () => {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
  const { email, password } = formData;
  const ctx = useContext(Context);
  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };
  const API_URL = process.env.REACT_APP_BACK_URL + `/api/users/login`;
  let navigate = useNavigate();
  const log = async () => {
    const res = await axios.post(API_URL, formData);
    if (res.data.token) {
      ctx.setIsLoggedIn(true);
      localStorage.setItem("token", res.data.token);
      localStorage.setItem("name", res.data.name);
      localStorage.setItem("isLoggedIn", true);
      localStorage.setItem("email", res.data.email);
      localStorage.setItem("_id", res.data._id);
      if (res.data.isAdmin === true) {
        localStorage.setItem("isAdmin", true);
        ctx.setIsAdmin(true);
      }
      // else {
      //   localStorage.setItem("isAdmin", false);
      //   ctx.setIsAdmin(false);
      // }
      navigate("/", { replace: true });
    }
    return res.data;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    log();
  };

  return (
    <>
      <div className={styles.Login__container}>
        <div className={styles.Login__sectionOne}>
          <img src={image1} alt="heroImage" className={styles.image1}></img>
        </div>
        <div className={styles.form__container}>
          <div className={styles.Login__sectionTwo}>
            <div className={styles.Login__border}>
              <h1 className={styles.heading}>Login</h1>
              <section className={styles.login__form}>
                <form onSubmit={onSubmit}>
                  <div className={styles.form__group}>
                    <input
                      style={{ textTransform: "none" }}
                      type="text"
                      id="email"
                      name="email"
                      value={email}
                      placeholder="Enter your email"
                      onChange={onChange}
                    />
                  </div>

                  <div className={styles.form__group}>
                    <input
                      type="password"
                      id="password"
                      name="password"
                      value={password}
                      placeholder="Enter your password"
                      onChange={onChange}
                    />
                  </div>

                  <div className={styles.form__group}>
                    <button type="submit" className={styles.login__btn}>
                      Submit
                    </button>
                  </div>
                </form>
              </section>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Login;
