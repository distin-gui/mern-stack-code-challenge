import React from "react";
import { Link } from "react-router-dom";
import styles from "./Footer.module.css";
import {
  FaFacebook,
  FaInstagram,
  FaWhatsapp,
  FaTwitter,
  FaPhoneAlt,
  FaHeart,
} from "react-icons/fa";
import { MdMailOutline, MdLocationPin } from "react-icons/md";
import logo from "../assets/images/chef.png";
const Footer = () => {
  return (
    <div className={styles.footer__container}>
      <div className={styles.section__one}>
        {" "}
        <div className={styles.logo}>
          <img
            src={logo}
            alt="Mad|Gamers"
            className={styles.logo}
            width="200px"
            height="150px"
          ></img>
        </div>
        <ul className={styles.footer__menu}>
          <div className={styles.footer__menu__title}>
            <h4>Links</h4>
          </div>
          <Link to="/">
            <li
              className={styles.Footer__li}
              onClick={() => {
                window.scrollTo(0, 0);
              }}
            >
              Home
            </li>
          </Link>
        </ul>
        <div className={styles.footer__contact}>
          <div className={styles.footer__contact__title}>
            <h4>Contact Us</h4>
          </div>
          <div className={styles.footer__icon}>
            <FaPhoneAlt size={25} />
            &nbsp;76-486811
          </div>
          <div className={styles.footer__icon}>
            <MdMailOutline size={30} />
            &nbsp;ahmadmoughrabi2020@gmail.com{" "}
          </div>
          <div className={styles.footer__icon}>
            <MdLocationPin size={30} />
            &nbsp;Beirut - Lebanon
          </div>
        </div>
      </div>

      <div className={styles.footer__qoute}>
        <p>
          Copyright © 2022 All Rights Reserved by MadMogh &nbsp;{"\n"}
          <u>Made with</u>
          {"       "}&nbsp;&nbsp;
          <FaHeart size={20} color={"red"} className={styles.footer__heart} />
          {"    "}
        </p>
        <div className={styles.footer__social}>
          <FaFacebook size={30} color={"#FFF"} />
          <FaInstagram size={30} color={"#FFF"} />
          <FaWhatsapp size={30} color={"#FFF"} />
          <FaTwitter size={30} color={"#FFF"} />
        </div>
      </div>
    </div>
  );
};

export default Footer;
