import { React, useState, useContext, useEffect } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import TextField from "@mui/material/TextField";
import DialogTitle from "@mui/material/DialogTitle";
import { AiFillEdit, AiTwotoneDelete } from "react-icons/ai";
import { Context } from "../store/auth-context";
import { toast } from "react-toastify";
import styles from "../pages/Admin.module.css";
import axios from "axios";
import { style } from "@mui/system";

const Table = ({
  key,
  name,
  price,
  description,
  image,
  id,
  reFetch,
  isCat,
  reFetch2,
  isMenu,
  catid,
}) => {
  const API_URL = process.env.REACT_APP_BACK_URL + "/api/";
  const token = localStorage.getItem("token");
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const ctx = useContext(Context);
  const [uploadedImage, setUploadedImage] = useState([]);

  const getCategories = async () => {
    const response = await axios.get(API_URL + "categories/", config);

    ctx.setData(response.data);
    return response.data;
  };

  useEffect(() => {
    getCategories();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // eslint-disable-next-line no-unused-vars
  const [formData, setFormData] = useState({
    name: name,
    price: price,
    description: description,
    image: image,
  });

  // const { itemName, itemPrice, itemDescription, itemImage } = { itemData };
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpen2 = () => {
    setOpen2(true);
  };

  const handleClose2 = () => {
    setOpen2(false);
  };
  const updateCatClick = async (e) => {
    e.preventDefault();
    const response = await axios.put(
      API_URL + "categories/" + id,
      formData,
      config
    );
    reFetch();
    handleClose();
    toast.success("Category Updated!");
    return response;
  };
  const updateItemClick = async (e) => {
    const response = await axios.put(API_URL + "items/" + id, formData, config);
    console.log(formData);
    reFetch2();
    handleClose();
    toast.success("Item Updated!");
    return response;
  };

  const imageChange = (e) => {
    e.preventDefault();
    if (e.target.files && e.target.files.length > 0) {
      setUploadedImage({
        imagePreview: URL.createObjectURL(e.target.files[0]),
        imageAsFile: e.target.files[0],
      });
      setFormData((params) => ({
        ...params,
        image: uploadedImage?.imageAsFile,
      }));
    }
  };
  const deleteCatClick = async (e) => {
    e.preventDefault();
    const response = await axios.delete(API_URL + "categories/" + id, config);
    reFetch();
    toast.error("Category Deleted");
    handleClose2();
    return response;
  };
  const removeItemClick = async (e) => {
    e.preventDefault();
    const response = await axios.put(
      API_URL + "categories/remove/" + catid,
      { id: id },
      config
    );
    toast.error("Item Removed");
    reFetch();
    handleClose2();
    return response;
  };
  const deleteItemClick = async (e) => {
    e.preventDefault();
    const response = await axios.delete(API_URL + "items/" + id, config);
    reFetch2();
    toast.error("Item Deleted");
    handleClose2();
    return response;
  };
  const reOrderClick = async (e) => {
    e.preventDefault();
  };
  // const deleteClick = async (e) => {
  //   e.preventDefault();
  //   if (price === undefined) {
  //     const response = await axios.delete(API_URL + "categories/" + id, config);
  //     reFetch();

  //     handleClose2();
  //     toast.error("deleted");
  //     return response;
  //   } else {
  //     const response = await axios.delete(API_URL + "items/" + id, config);
  //     reFetch();

  //     handleClose2();
  //     toast.error("deleted");

  //     return response;
  //   }
  // };
  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
    console.log(e.target.name);
    console.log(e.target.value);
  };

  const overflow = {
    overflow: "hidden",
    overflowY: "hidden",
    overflowX: "hidden",
    scrollbarWidth: "none" /* Firefox */,
  };
  return (
    <>
      {isCat === true ? (
        <>
          <tr key={key} id={id}>
            <td data-label="Name">{name}</td>

            <td key={key} id={id} data-label="Actions">
              <AiFillEdit
                size={30}
                title="Edit Category"
                className="iconss__func"
                onClick={handleClickOpen}
                color="GREEN"
                cursor="pointer"
                id={id}
              />
            </td>
            <td key={key} id={id} data-label="Actions">
              <AiTwotoneDelete
                onClick={handleClickOpen2}
                size={30}
                title="Delete Category"
                className="iconss__func"
                color="#D11A2A"
                cursor="pointer"
                id={id}
              />
            </td>
          </tr>
          <form>
            <Dialog
              fullWidth={true}
              sx={{ overflowY: "scroll" }}
              open={open}
              onClose={handleClose}
            >
              <DialogTitle>
                <h2>Edit</h2>
              </DialogTitle>
              <DialogContent style={overflow}>
                <TextField
                  autoFocus
                  margin="dense"
                  fullWidth
                  defaultValue={name}
                  variant="standard"
                  label="Name"
                  type="text"
                  id="name"
                  name="name"
                  onChange={onChange}
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={updateCatClick}>Edit</Button>
              </DialogActions>
            </Dialog>
          </form>
          <form>
            <Dialog
              maxWidth={"md"}
              sx={{ overflowY: "scroll" }}
              open={open2}
              onClose={handleClose}
            >
              <DialogContent>
                <p>Are you sure you want to delete it?</p>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose2}>Cancel</Button>
                <Button onClick={deleteCatClick}>Delete</Button>
              </DialogActions>
            </Dialog>
          </form>
        </>
      ) : (
        <>
          {isMenu === true ? (
            <>
              <tr key={key} id={id}>
                <td data-label="Name">{name}</td>

                <td key={key} id={id} data-label="Actions">
                  <AiFillEdit
                    size={30}
                    title="Reorder item in category"
                    className="iconss__func"
                    onClick={handleClickOpen}
                    color="GREEN"
                    cursor="pointer"
                    id={id}
                  />
                </td>
                <td key={key} id={id} data-label="Actions">
                  <AiTwotoneDelete
                    onClick={handleClickOpen2}
                    title="Delete item from menu"
                    size={30}
                    className="iconss__func"
                    color="#D11A2A"
                    cursor="pointer"
                    id={id}
                  />
                </td>
              </tr>
              <form>
                <Dialog
                  fullWidth={true}
                  sx={{ overflowY: "scroll" }}
                  open={open}
                  onClose={handleClose}
                >
                  <DialogTitle>
                    <h2>Rorder</h2>
                  </DialogTitle>
                  <DialogContent style={overflow}>
                    <TextField
                      autoFocus
                      margin="dense"
                      fullWidth
                      variant="standard"
                      // defaultValue={name}
                      label="Name"
                      type="text"
                      id="name"
                      name="name"
                      // value={name}
                      onChange={onChange}
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={reOrderClick}>Rorder</Button>
                  </DialogActions>
                </Dialog>
              </form>
              <form>
                <Dialog
                  maxWidth={"md"}
                  sx={{ overflowY: "scroll" }}
                  open={open2}
                  onClose={handleClose2}
                >
                  <DialogContent>
                    <p>Are you sure you want to delete it?</p>
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleClose2}>Cancel</Button>
                    <Button onClick={removeItemClick}>Delete</Button>
                  </DialogActions>
                </Dialog>
              </form>
            </>
          ) : (
            <>
              <tr key={key} id={id}>
                <td data-label="Name">{name}</td>

                <td key={key} id={id} data-label="Actions">
                  <AiFillEdit
                    size={30}
                    title="Edit Item"
                    className="iconss__func"
                    onClick={handleClickOpen}
                    color="GREEN"
                    cursor="pointer"
                    id={id}
                  />
                </td>
                <td key={key} id={id} data-label="Actions">
                  <AiTwotoneDelete
                    title="Delete Item"
                    onClick={handleClickOpen2}
                    size={30}
                    className="iconss__func"
                    color="#D11A2A"
                    cursor="pointer"
                    id={id}
                  />
                </td>
              </tr>
              <form>
                <Dialog
                  fullWidth={true}
                  sx={{ overflowY: "scroll" }}
                  open={open}
                  onClose={handleClose}
                >
                  <DialogTitle>
                    <h2>Edit</h2>
                  </DialogTitle>
                  <DialogContent>
                    <TextField
                      margin="dense"
                      fullWidth
                      label="Name"
                      variant="standard"
                      type="text"
                      className="form-control"
                      id="name"
                      name="name"
                      defaultValue={name}
                      onChange={onChange}
                    />
                  </DialogContent>{" "}
                  <DialogContent>
                    <TextField
                      margin="dense"
                      fullWidth
                      label="Price"
                      variant="standard"
                      type="number"
                      className="form-control"
                      id="price"
                      name="price"
                      defaultValue={price}
                      onChange={onChange}
                    />
                  </DialogContent>{" "}
                  <DialogContent>
                    <TextField
                      margin="dense"
                      fullWidth
                      label="Description"
                      variant="standard"
                      type="text"
                      className="form-control"
                      id="description"
                      name="description"
                      defaultValue={description}
                      onChange={onChange}
                    />
                  </DialogContent>{" "}
                  <DialogContent style={overflow}>
                    <TextField
                      type="file"
                      id="image"
                      label="Add Image"
                      name="image"
                      accept="image/*"
                      autoFocus
                      margin="dense"
                      fullWidth
                      variant="standard"
                      onInput={imageChange}
                      onChange={imageChange}
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={updateItemClick}>Edit</Button>
                  </DialogActions>
                </Dialog>
              </form>
              <form>
                <Dialog
                  maxWidth={"md"}
                  sx={{ overflowY: "scroll" }}
                  open={open2}
                  onClose={handleClose}
                >
                  <DialogContent>
                    <p>Are you sure you want to delete it?</p>
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleClose2}>Cancel</Button>
                    <Button onClick={deleteItemClick}>Delete</Button>
                  </DialogActions>
                </Dialog>
              </form>
            </>
          )}
        </>
      )}
    </>
  );
};

export default Table;
