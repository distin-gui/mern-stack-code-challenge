import React from "react";
import ItemList from "../components/ItemList";
import styles from "./Items.module.css";
import { Element } from "react-scroll";
const Items = ({ catName, name, price, description, id, image }) => {
  return (
    <>
      <Element>
        <div
          id={catName}
          title={catName}
          className={styles.catItems__container}
        >
          <ul className={styles.items__ul}>
            {/* {catItems.map((item) => ( */}
            <ItemList
              key={id}
              id={id}
              name={name}
              image={image}
              price={price}
              description={description}
            />
            {/* ))} */}
          </ul>
        </div>
      </Element>
    </>
  );
};

export default Items;
