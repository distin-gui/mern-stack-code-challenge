import React from "react";
import img1 from "../assets/images/platters.png";
import img2 from "../assets/images/pastas.png";
import img3 from "../assets/images/beverages.png";
import img4 from "../assets/images/appetizers.png";
import styles from "./Menu.module.css";
// eslint-disable-next-line no-unused-vars
import { Link, animateScroll as scroll } from "react-scroll";

const Menu = ({ catName }) => {
  // const handleScroll = () => {
  //   console.log("hi");
  // };
  const getIcon = (currentCat = "") => {
    switch (currentCat) {
      case "Platters":
        return (
          <img src={img1} width="150px" height="150px" alt="Platters"></img>
        );
      case "Pastas":
        return <img src={img2} width="150px" height="150px" alt="Pastas"></img>;

      case "Beverages":
        return (
          <img src={img3} width="150px" height="150px" alt="Beverages"></img>
        );

      case "Appetizers":
        return (
          <img src={img4} width="150px" height="150px" alt="Appetizers"></img>
        );

      default:
        return (
          <img src={img1} width="150px" height="150px" alt="Platters"></img>
        );
    }
  };
  return (
    <>
      <Link
        className={styles.menu__li}
        to={catName}
        // activeClass={styles.menu__li}
        spy={true}
        smooth={true}
        hashSpy={true}
        offset={-100}
        duration={500}
        delay={500}
        isDynamic={true}
      >
        {/* <li className={styles.menu__li}> */}
        <div className={styles.menu__img}>{getIcon(catName)}</div>
        <h2 className={styles.menu__h2}>{catName}</h2>
        {/* </li> */}
      </Link>
    </>
  );
};

export default Menu;
