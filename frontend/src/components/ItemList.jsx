import React from "react";
import styles from "./Items.module.css";

const itemList = ({ name, image, description, price }) => {
  return (
    <>
      <li className={styles.items__li}>
        <div className={styles.item__title}>
          <img src={image} alt="item" width="150px" height="150px"></img>
          <h2 className={styles.item__h2}>{name}</h2>
        </div>
        <h4 className={styles.item__h4}>{description}</h4>
        <p className={styles.items__price}>
          <b>Price: {price}</b>
        </p>
      </li>
    </>
  );
};

export default itemList;
