import { React, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import logo from "../assets/images/chef.png";
import profilePic from "../assets/images/profilepic.jpeg";
import styles from "./Header.module.css";
import { Dropdown } from "rsuite";
import { Context } from "../store/auth-context";
const Header = () => {
  const navigate = useNavigate();
  const handleLogin = (e) => {
    e.preventDefault();
    navigate("/login");
  };
  const handleRegister = (e) => {
    e.preventDefault();
    navigate("/register");
  };

  const ctx = useContext(Context);
  const onLogoutHandler = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("_id");
    localStorage.removeItem("name");
    localStorage.removeItem("isLoggedIn");
    localStorage.removeItem("image");
    localStorage.removeItem("email");
    localStorage.removeItem("email");
    localStorage.removeItem("isAdmin");
    ctx.setIsLoggedIn(false);
    ctx.setIsAdmin(false);
    navigate("/");
  };
  const handleAdminDash = (e) => {
    e.preventDefault();
    navigate("/admin");
  };
  // ctx.isLoggedIn == true ?    conditional rendering
  return (
    <nav className={styles.navbar}>
      <div className={styles.logo}>
        <Link to="/">
          <img
            src={logo}
            alt="Gaming"
            className={styles.logo}
            width="3.7em"
            height="3.7em"
            float="left"
            margin-left="0.5em"
            position="relative"
          ></img>
        </Link>
      </div>

      <div className={styles.navbar__right}>
        <div className="button-bar">
          {ctx.isLoggedIn === false ? (
            <>
              {" "}
              <button onClick={handleLogin}>Login</button>
              <button onClick={handleRegister}>Register</button>
            </>
          ) : (
            <>
              <div className={styles.Header__right__image}>
                <Dropdown
                  icon={
                    <img
                      style={{
                        position: "relative",
                        width: "40px",
                        borderRadius: "50%",
                        zIndex: "6",
                        right: "1rem",
                        height: "40px",
                        objectFit: "fill",
                      }}
                      src={profilePic}
                      alt="pp"
                    ></img>
                  }
                  style={{ position: "absolute", right: "0", top: "5px" }}
                  title={localStorage.getItem("name")}
                >
                  {ctx.isAdmin && (
                    <Dropdown.Item>
                      <button onClick={handleAdminDash}>Dashboard</button>
                    </Dropdown.Item>
                  )}
                  <Dropdown.Item>
                    <button onClick={onLogoutHandler}>Logout</button>
                  </Dropdown.Item>
                </Dropdown>
              </div>
            </>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Header;
