import { Navigate, Outlet } from "react-router-dom";
const AdminProtectedRoutes = ({ user }) => {
  if (!user.isAdmin) {
    return <Navigate to="/" replace />;
  }

  return <Outlet />;
};
export default AdminProtectedRoutes;
