import asyncHandler from "express-async-handler";
import Item from "../models/itemModel.js";

// @desc    Create an Item
// @route   POST /api/items
// @access  Private/Admin

const createItem = asyncHandler(async (req, res) => {
  const { name, image, description, price } = req.body;

  if (!name) {
    res.status(400);
    throw new Error(" item name  must be filled.");
  }

  const item = new Item({
    name,
    image,
    description,
    price,
  });
  const createdItem = await item.save();
  res.status(201).json(createdItem);
});

// @desc    Fetch all items
// @route   GET /api/items
// @access  Public

const getItems = asyncHandler(async (req, res) => {
  const item = await Item.find({});
  if (!item) {
    res.status(404);
    throw new Error("No Items found.");
  }
  res.json(item);
});

// @desc    Update Item
// @route   PUT /api/items/:id
// @access  Private/Admin

const updateItem = asyncHandler(async (req, res) => {
  const { name, price, description, image } = req.body;
  const item = await Item.findById(req.params.id);
  if (!item) {
    res.status(404);
    throw new Error("Item not found.");
  }
  if (!name) {
    res.status(400);
    throw new Error("Item name must be filled.");
  }
  item.name = name;
  item.price = price;
  item.description = description;
  if (req.file) {
    item.image = req.file.path;
  }
  const updatedItem = await item.save();
  res.json(updatedItem);
});

// @desc    Delete Item
// @route   DELETE /api/items/:id
// @access  Private/Admin

const deleteItem = asyncHandler(async (req, res) => {
  const item = await Item.findById(req.params.id);

  if (item) {
    await item.remove();
    res.json({ message: "item removed" });
  } else {
    res.status(404);
    throw new Error("item not found");
  }
});

export { getItems, createItem, updateItem, deleteItem };
