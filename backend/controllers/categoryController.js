import asyncHandler from "express-async-handler";
import Category from "../models/categoryModel.js";

// @desc    Fetch all categories
// @route   GET /api/categories
// @access  Public
const getCategories = asyncHandler(async (req, res) => {
  const category = await Category.find({}).populate("items");
  if (!category) {
    res.status(404);
    throw new Error("No Categories found.");
  }
  res.json(category);
});

// @desc    Fetch single category
// @route   GET /api/categories/:id
// @access  Public
const getCategoryById = asyncHandler(async (req, res) => {
  // const id = mongoose.Types.ObjectId(req.params.id);
  const category = await Category.findById(req.params.id);

  if (category) {
    res.json(category);
  } else {
    res.status(404);
    throw new Error("Category not found");
  }
});

// @desc    Create a category
// @route   POST /api/categories
// @access  Private/Admin
const createCategory = asyncHandler(async (req, res) => {
  const { name, items } = req.body;

  if (!name) {
    res.status(400);
    throw new Error("Category name should be filled ");
  }

  const category = new Category({
    name,
    items,
  });
  const createdCategory = await category.save();
  res.status(201).json(createdCategory);
});

// @desc    Delete a category
// @route   DELETE /api/categories/:id
// @access  Private/Admin
const deleteCategory = asyncHandler(async (req, res) => {
  const category = await Category.findById(req.params.id);

  if (category) {
    await category.remove();
    res.json({ message: "Category removed" });
  } else {
    res.status(404);
    throw new Error("Category not found");
  }
});

// @desc    Delete item in category
// @route   DELETE /api/categories/:id
// @access  Private/Admin

// @desc    Update a category
// @route   PUT /api/categories/:id
// @access  Private/Admin
const updateCategory = asyncHandler(async (req, res) => {
  const { name } = req.body;

  const category = await Category.findById(req.params.id);
  if (!category) {
    res.status(404);
    throw new Error("Category not found.");
  }

  category.name = name;

  const updatedCategory = await category.save();
  res.json(updatedCategory);
});

const updateCatItems = asyncHandler(async (req, res) => {
  const { items } = req.body;
  const category = await Category.findById(req.params.id);
  if (!category) {
    res.status(404);
    throw new Error("Category not found");
  }

  for (var i = 0; i < items.length; i++) {
    category.items.push(items[i].id);
  }
  const updatedCategory = await category.save();
  res.json(updatedCategory);
});

const deleteCatItems = asyncHandler(async (req, res) => {
  const { id } = req.body;
  const category = await Category.findById(req.params.id);
  var temp = [];
  temp = category.items;
  for (var i = 0; i < temp.length; i++) {
    if (temp[i].toString() === id) {
      temp.splice(i, 1);
    }
  }

  const updatedCategory = await Category.findByIdAndUpdate(req.params.id, {
    items: temp,
  });
  const newCat = await updatedCategory.save();
  res.json(newCat);
});

export {
  getCategoryById,
  getCategories,
  deleteCategory,
  createCategory,
  updateCategory,
  updateCatItems,
  deleteCatItems,
};
