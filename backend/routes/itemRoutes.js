import express from "express";
const router = express.Router();

import {
  createItem,
  getItems,
  deleteItem,
  updateItem,
} from "../controllers/itemController.js";
import { protect, admin } from "../middlewares/authMiddleware.js";
import { upload } from "../middlewares/uploadMiddleware.js";

router.route("/items").get(getItems);
router
  .route("/")
  .get(getItems)
  .post(protect, admin, upload.single("image"), createItem);
router
  .route("/:id")
  .delete(protect, admin, deleteItem)
  .put(protect, admin, upload.single("image"), updateItem);
export default router;
