import express from "express";
const router = express.Router();
import {
  getCategoryById,
  getCategories,
  deleteCategory,
  createCategory,
  updateCategory,
  updateCatItems,
  deleteCatItems,
} from "../controllers/categoryController.js";
import { protect, admin } from "../middlewares/authMiddleware.js";
import { upload } from "../middlewares/uploadMiddleware.js";
router.route("/categories").get(getCategories);
router
  .route("/")
  .get(getCategories)
  .post(protect, admin, upload.single("image"), createCategory);
// array("image")
router
  .route("/:id")
  .get(getCategoryById)
  .delete(protect, admin, deleteCategory)

  .put(protect, admin, upload.single("image"), updateCategory);
router.route("/edit/:id").put(protect, admin, updateCatItems);
router.route("/remove/:id").put(protect, admin, deleteCatItems);

export default router;
